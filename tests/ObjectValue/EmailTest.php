<?php

declare(strict_types=1);

namespace ObjectValue\Tests\ObjectValue;

use Assert\AssertionFailedException;
use Assert\InvalidArgumentException;
use Faker\Factory;
use ObjectValue\ObjectValue\Email;
use PHPUnit\Framework\TestCase;

class NewsletterGeneratorTest extends TestCase
{
    /**
     * @throws AssertionFailedException
     */
    public function testCreateSuccessful()
    {
        $faker = Factory::create();
        $email = $faker->email();

        $emailObject = Email::createFromString($email);

        $this->assertInstanceOf(Email::class, $emailObject);
        $this->assertEquals($email, $emailObject);
        $this->assertEquals($email, $emailObject->getValue());
        $this->assertFalse($emailObject->isNull());
    }

    /**
     * @throws AssertionFailedException
     */
    public function testCreateFailOnFormat()
    {
        $this->expectException(InvalidArgumentException::class);
        Email::createFromString("wrong-email@");
    }

    /**
     * @throws AssertionFailedException
     */
    public function testReturnDomainCorrectly()
    {
        $emailObject = Email::createFromString("test@test.fr");

        $this->assertEquals("test.fr", $emailObject->getDomain());
    }
}
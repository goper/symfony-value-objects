#!/usr/bin/env bash

touch .env

DOCKER_TERM_OPTS="--init"

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. "$DIR/cli-setup.sh"

composer install

php vendor/bin/phpunit --coverage-text --colors=never
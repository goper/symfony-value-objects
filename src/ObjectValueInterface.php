<?php

declare(strict_types=1);

namespace ObjectValue;

interface ObjectValueInterface
{
    public function __toString(): string;
}

<?php

declare(strict_types=1);

namespace ObjectValue\ObjectValue;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\ORM\Mapping as ORM;
use ObjectValue\ObjectValueInterface;

#[ORM\Embeddable]
class Email implements ObjectValueInterface
{
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $value;

    private function __construct()
    {
    }

    public function isNull(): bool
    {
        return null === $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getDomain(): string
    {
        return explode('@', $this->value)[1];
    }

    /**
     * @throws AssertionFailedException
     */
    public static function createFromString(?string $email): self
    {
        Assertion::email($email);

        $obj = new self();
        $obj->value = $email;

        return $obj;
    }
}
